# Java template for homeworks in B4B33ALG
contains:
* **FastIO** - faster input reading/writing than normal Java reader/writers
* **PubTest** - compares programme's output with datapub output, packs passing solution into zip
* **Main** - solution skeleton
