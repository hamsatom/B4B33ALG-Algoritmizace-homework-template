package alg;

import java.io.IOException;

public class Main {

  public static void main(String[] args) throws IOException {
    InputReader in = new InputReader(System.in);

    int i = in.readInt();

    OutputWriter out = new OutputWriter(System.out);
    out.printLine(i);
    out.close();
  }
}
