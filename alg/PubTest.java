package alg;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 08.03.2017.
 */
public class PubTest {

	// CHANGE THESE TO MATCH PATHS IN YOUR SYSTEM
	private static final String DATAPUB_LOCATION = ".\\datapub\\pub";
	private static final String ALG_LOCATION = ".\\src";

	private static final Pattern FILENAME_FILTER = Pattern.compile("[:T]");
	private static boolean allPassed = true;

	@Test(priority = 1, timeOut = 8000)
	public void testPub1() throws IOException {
		solvePub("01");
	}

	@Test(priority = 2, timeOut = 8000)
	public void testPub2() throws IOException {
		solvePub("02");
	}

	@Test(priority = 3, timeOut = 8000)
	public void testPub3() throws IOException {
		solvePub("03");
	}

	@Test(priority = 4, timeOut = 8000)
	public void testPub4() throws IOException {
		solvePub("04");
	}

	@Test(priority = 5, timeOut = 8000)
	public void testPub5() throws IOException {
		solvePub("05");
	}

	@Test(priority = 6, timeOut = 8000)
	public void testPub6() throws IOException {
		solvePub("06");
	}

	@Test(priority = 7, timeOut = 8000)
	public void testPub7() throws IOException {
		solvePub("07");
	}

	@Test(priority = 8, timeOut = 8000)
	public void testPub8() throws IOException {
		solvePub("08");
	}

	@Test(priority = 9, timeOut = 8000)
	public void testPub9() throws IOException {
		solvePub("09");
	}

	@Test(priority = 10, timeOut = 8000)
	public void testPub10() throws IOException {
		solvePub("10");
	}

	@AfterTest
	public void zipSolution() throws IOException {
		if (!allPassed) {
			return;
		}

		Path algDir = toPath(ALG_LOCATION, "alg");
		System.out.println("Located alg directory in: " + algDir);
		Path base = toPath(ALG_LOCATION);
		String zipName = FILENAME_FILTER.matcher("solution-" + LocalDateTime.now() + ".zip").replaceAll("-");
		Path outputFile = toPath(ALG_LOCATION, zipName);
		System.out.println("Packing solution into zip: " + outputFile);

		try (ZipOutputStream zip = new ZipOutputStream(new BufferedOutputStream(Files.newOutputStream(outputFile)));
			 Stream<Path> fileTree = Files.walk(base)
		) {
			fileTree.filter(path -> !path.toFile().isDirectory())
					.filter(path -> path.startsWith(algDir))
					.filter(path -> !path.endsWith("PubTest.java"))
					.forEach(path -> {
						ZipEntry zipEntry = new ZipEntry(base.relativize(path).toString());
						try {
							zip.putNextEntry(zipEntry);
							Files.copy(path, zip);
							zip.closeEntry();
						} catch (IOException e) {
							throw new IllegalStateException(e);
						}
					});
		}
	}

	private static void solvePub(String pubNumber) throws IOException {
		System.out.println("Running pub test " + pubNumber);
		Path pubInFile = toPath(DATAPUB_LOCATION + pubNumber + ".in");
		System.out.println("Pub in file: " + pubInFile);
		Path pubOutFile = toPath(DATAPUB_LOCATION + pubNumber + ".out");
		System.out.println("Pub out file: " + pubOutFile);

		String expectedOutput = new String(Files.readAllBytes(pubOutFile), StandardCharsets.UTF_8);
		System.setIn(new BufferedInputStream(Files.newInputStream(pubInFile)));

		PrintStream previousStdout = System.out;
		try {
			long duration;
			String actualResult;
			try (ByteArrayOutputStream buffer = new ByteArrayOutputStream(expectedOutput.length());
				 PrintStream newStdout = new PrintStream(new BufferedOutputStream(buffer, expectedOutput.length()), true, StandardCharsets.UTF_8.name())
			) {
				System.setOut(newStdout);

				long startTime = System.nanoTime();
				Main.main(null);
				duration = System.nanoTime() - startTime;
				actualResult = buffer.toString(StandardCharsets.UTF_8.name());
			} finally {
				System.setOut(previousStdout);
			}

			System.out.printf("Duration: %f seconds%n", duration / 1000000000.0);
			Assert.assertEquals(actualResult.trim(), expectedOutput.trim(), "Incorrect solution");
		} catch (Throwable e) {
			allPassed = false;
			throw e;
		}
	}

	private static Path toPath(String first, String... more) {
		return Paths.get(first, more).toAbsolutePath().normalize();
	}
}